# Web tech project

---

## Introduction

* What is the main need/problem this product solves?

       The application must allow students to organise their notes by classes they attend and individual study activities


* To whom this product addresses?

   	  Our app is destined for students.
	  

* What other similar existing products are there on the market?

     [Evernote](https://evernote.com/)

     [StuDocu](https://www.studocu.com/)

---

## Technologies used

The app is based on a Single Page App architecture. Our fontend is developed using the ReactJS framework and the backend has the ExpressJS. In the backend, the application follows the RESTful APIs concepts. The data is stored in a MySQL database using the Sequelize ORM for manipulation.
