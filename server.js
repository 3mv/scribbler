'use strict'
const express = require('express');
const bodyParser = require('body-parser');
const db = require('./config/database');

const app = express();


// Init Middleware
app.use(bodyParser.json({ extended: false }));

app.get('/', (req, res) => {
  res.status(200).json({ message: 'API Running' });
});

// Define Routes
app.use('/api/users', require('./routes/api/users'));
app.use('/api/notebooks', require('./routes/api/notebooks'));

// Error middleware
app.use(require('./middleware/error'));


const PORT = process.env.PORT || 8080;

db.sequelize.sync().then(() => {
  console.log('Database connected and synced');
  app.listen(PORT, () => {
    console.log(`Server started on port ${PORT}`);
  })
});
