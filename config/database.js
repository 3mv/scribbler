"use strict";

const fs = require("fs");
const path = require("path");
const Sequelize = require("sequelize");
const config = require("config");
const mysql = require("mysql2/promise");

const db = {};

const DB_NAME = config.get("dbName");
const DB_USER = config.get("dbUser");
const DB_PASSWORD = config.get("dbPassword");

// Creare baza de data daca nu exista
mysql
  .createConnection({
    user: DB_USER,
    password: DB_PASSWORD
  })
  .then(async connection => {
    await connection.query(`CREATE DATABASE IF NOT EXISTS ${DB_NAME}`);
  });

let sequelize = new Sequelize(DB_NAME, DB_USER, DB_PASSWORD, {
  dialect: "mysql"
});

const DIR_NAME = path.join(__dirname, "../models/");

fs.readdirSync(DIR_NAME)
  .filter(file => {
    return file.indexOf(".") !== 0 && file.slice(-3) === ".js";
  })
  .forEach(file => {
    const model = sequelize["import"](path.join(DIR_NAME, file));
    db[model.name] = model;
  });

Object.keys(db).forEach(modelName => {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;
