import { combineReducers } from "redux";
import alert from "./alert";
import auth from "./auth";
import notebook from "./notebook";
import note from "./note";

const rootReducer = combineReducers({
  alert,
  auth,
  notebook,
  note
});

export default rootReducer;
