import {
  GET_NOTEBOOKS,
  NOTEBOOK_ERROR,
  DELETE_NOTEBOOK,
  ADD_NOTEBOOK,
  GET_NOTEBOOK,
  UPDATE_NOTEBOOK_SUCCES,
  UPDATE_NOTEBOOK_FAIL,
  UPDATE_NOTEBOOK_SHARE_SUCCES
} from "../actions/types";

const initialState = {
  notebooks: [],
  notebook: null,
  loading: true,
  error: {}
};

export default function(state = initialState, action) {
  const { type, payload } = action;

  switch (type) {
    case GET_NOTEBOOKS:
      return {
        ...state,
        notebooks: payload,
        loading: false
      };
    case GET_NOTEBOOK:
      return {
        ...state,
        notebook: payload,
        loading: false
      };
    case ADD_NOTEBOOK:
      return {
        ...state,
        notebooks: [...state.notebooks, payload],
        loading: false
      };
    case UPDATE_NOTEBOOK_SUCCES:
    case UPDATE_NOTEBOOK_SHARE_SUCCES:
      return {
        ...state,
        notebooks: state.notebooks.map(n => {
          if (n.id === payload.id) {
            return payload;
          } else {
            return n;
          }
        }),
        loading: false
      };
    case DELETE_NOTEBOOK:
      return {
        ...state,
        notebooks: state.notebooks.filter(
          notebook => notebook.title !== payload
        ),
        loading: false
      };
    case NOTEBOOK_ERROR:
    case UPDATE_NOTEBOOK_FAIL:
      return {
        ...state,
        error: payload,
        loading: false
      };

    default:
      return state;
  }
}
