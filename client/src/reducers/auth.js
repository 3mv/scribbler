import {
  REGISTER_SUCCES,
  REGISTER_FAIL,
  USER_LOADED,
  AUTH_ERROR,
  LOGIN_SUCCES,
  LOGIN_FAIL,
  LOGOUT,
  USER_UPDATE_SUCCES,
  USER_UPDATE_FAIL,
  GET_USERS_SUCCES,
  GET_USERS_ERROR
} from "../actions/types";

const initialState = {
  token: localStorage.getItem("token"),
  isAuthenticated: null,
  loading: true,
  user: null,
  users: []
};

export default function(state = initialState, action) {
  const { type, payload } = action;

  switch (type) {
    case REGISTER_SUCCES:
      localStorage.setItem("token", payload.token);
      return {
        ...state,
        ...payload,
        isAuthenticated: false,
        loading: true
      };
    case LOGIN_SUCCES:
      localStorage.setItem("token", payload.token);
      return {
        ...state,
        ...payload,
        isAuthenticated: false,
        loading: true
      };
    case REGISTER_FAIL:
    case AUTH_ERROR:
    case LOGIN_FAIL:
    case LOGOUT:
      localStorage.removeItem("token");
      return {
        ...state,
        token: null,
        isAuthenticated: false,
        loading: false
      };
    case USER_LOADED:
    case USER_UPDATE_SUCCES:
      return {
        ...state,
        isAuthenticated: true,
        loading: false,
        user: payload
      };
    case GET_USERS_SUCCES:
      return {
        ...state,
        users: payload,
        loading: false
      };
    case GET_USERS_ERROR:
    case USER_UPDATE_FAIL:
    default:
      return state;
  }
}
