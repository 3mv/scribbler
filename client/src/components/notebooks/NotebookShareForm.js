import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { updateNotebookShare } from "../../actions/notebook";
import { getUsers } from "../../actions/auth";
import Spinner from "../layout/Spinner";

const NotebookShareForm = ({
  updateNotebookShare,
  getUsers,
  users,
  notebook,
  onSave
}) => {
  useEffect(() => {
    getUsers();
  }, [getUsers]);

  const [formData, setFormData] = useState({
    usersArray: [],
    usersNames: [],
    selection: -1,
    selectionName: "Select"
  });

  // const remove = e => {
  //   e.preventDefault();

  //   setFormData({
  //     ...formData,
  //     usersArray: formData.usersArray.filter(u => u !== e.target.value)
  //   });
  // };

  const share = e => {
    if (formData.selection !== -1) {
      setFormData({
        ...formData,
        usersArray: [...formData.usersArray, formData.selection],
        usersNames: [...formData.usersNames, formData.selectionName],
        selection: -1,
        selectionName: "Select"
      });
    }
  };

  const save = () => {
    // remove the pop-up
    onSave();

    // save and send to back-end
    updateNotebookShare({
      title: notebook.title,
      users: formData.usersArray
    });
  };

  const selectionChange = e => {
    let id = e.target.value.split(",")[0];
    let name = e.target.value.split(",")[1];
    setFormData({
      ...formData,
      selection: id,
      selectionName: name
    });
  };

  return users.length === 0 ? (
    <Spinner />
  ) : (
    <div className='notebookItem__edit__share'>
      <div className='notebookItem__edit__share__inner'>
        <button onClick={onSave} className='notebookItem__edit__share__close'>
          X
        </button>
        <div className='notebookItem__edit__share__ad'>
          <select
            value={formData.selection + "," + formData.selectionName}
            onChange={selectionChange}
          >
            <option value='-1,Select'>Select</option>
            {users.map((u, index) => {
              return (
                <option key={index} value={u.id + "," + u.name}>
                  {u.name}
                </option>
              );
            })}
          </select>
          <button onClick={share}>Share</button>
        </div>
        <h3>Users:</h3>
        <ul>
          {formData.usersNames.map((u, index) => {
            return <li key={index}>{u}</li>;
          })}
        </ul>
        <button className='btn btn--primary' onClick={save}>
          Save share preferences
        </button>
      </div>
    </div>
  );
};

NotebookShareForm.propTypes = {
  updateNotebookShare: PropTypes.func.isRequired,
  notebook: PropTypes.object.isRequired,
  onSave: PropTypes.func.isRequired,
  users: PropTypes.array.isRequired
};

const mapStateToProps = state => ({
  users: state.auth.users
});

export default connect(mapStateToProps, { updateNotebookShare, getUsers })(
  NotebookShareForm
);
