import React, { Fragment, useState } from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import Moment from "react-moment";
import { connect } from "react-redux";
import { deleteNotebook, updateNotebook } from "../../actions/notebook";

const NotebookItem = ({
  auth,
  deleteNotebook,
  updateNotebook,
  onSelection,
  notebook: { id, title, category, createdAt, updatedAt, userId, shared }
}) => {
  const [notebookData, setNotebookData] = useState({
    title: title,
    category: category,
    editClass: "",
    shareClass: ""
  });

  const onChange = e => {
    setNotebookData({
      ...notebookData,
      [e.target.name]: e.target.value
    });
  };

  const activateEditMode = () => {
    setNotebookData({
      ...notebookData,
      editClass: "active"
    });
  };

  const edit = () => {
    setNotebookData({
      ...notebookData,
      editClass: ""
    });

    updateNotebook({
      id: id,
      title: notebookData.title,
      category: notebookData.category
    });
  };

  const noteb = { title: title, shared: shared };

  return (
    <Fragment>
      <div className='notebookItem'>
        <span className='notebookItem__data notebookItem__data--title'>
          <i className='fas fa-book'></i>
          {title}
        </span>
        <span className='notebookItem__data notebookItem__data--category'>
          {category}
        </span>
        <span className='notebookItem__data notebookItem__data--created'>
          <Moment format='DD/MM/YYYY'>{createdAt}</Moment>
        </span>
        <span className='notebookItem__data notebookItem__data--updated'>
          <Moment format='DD/MM/YYYY'>{updatedAt}</Moment>
        </span>

        <span className='notebookItem__data notebookItem__data--buttons'>
          {!auth.loading && userId === auth.user.id && (
            <Fragment>
              <button
                onClick={e => deleteNotebook(title)}
                type='button'
                className='notebookItem__data--buttons--delete'
              >
                <i className='fas fa-trash-alt'></i>
              </button>
              <button
                onClick={activateEditMode}
                type='button'
                className='notebookItem__data--buttons--edit'
              >
                <i className='fas fa-edit'></i>
              </button>
              <button
                type='button'
                className='notebookItem__data--buttons--share'
                onClick={() => {
                  onSelection(noteb);
                }}
              >
                <i className='fas fa-share'></i>
              </button>
            </Fragment>
          )}
          <Link to={`/notebooks/${title}`}>
            <i className='fas fa-folder-open'></i>
          </Link>
        </span>
      </div>
      <div className='notebookItem__edit '>
        <div
          className={"notebookItem__edit__content " + notebookData.editClass}
        >
          <input
            type='text'
            name='title'
            value={notebookData.title}
            onChange={onChange}
          />
          <input
            type='text'
            name='category'
            value={notebookData.category}
            onChange={onChange}
          />
          <button onClick={edit}>Edit</button>
        </div>
        {/* <NotebookShareForm notebook={noteb} onSave={onShareSaved} /> */}
      </div>
      <div className='rapid-fix'></div>
    </Fragment>
  );
};

NotebookItem.propTypes = {
  notebook: PropTypes.object.isRequired,
  auth: PropTypes.object.isRequired,
  deleteNotebook: PropTypes.func.isRequired,
  updateNotebook: PropTypes.func.isRequired,
  onSelection: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth
});

export default connect(mapStateToProps, { deleteNotebook, updateNotebook })(
  NotebookItem
);
