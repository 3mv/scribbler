import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { getNotebooks } from "../../actions/notebook";
import Spinner from "../layout/Spinner";
import NotebookItem from "./NotebookItem";
import NotebookForm from "./NotebookForm";
import NotebookShareForm from "./NotebookShareForm";

const Notebooks = ({ getNotebooks, notebook: { notebooks, loading } }) => {
  useEffect(() => {
    getNotebooks();
  }, [getNotebooks]);

  const [queryData, setQueryData] = useState({
    queryTitle: "",
    queryCategory: "",
    selectedNotebookForShare: null
  });

  const onSelectNotebookForShare = notebook => {
    setQueryData({
      ...queryData,
      selectedNotebookForShare: notebook
    });
  };

  const onShareSaved = () => {
    setQueryData({
      ...queryData,
      selectedNotebookForShare: null
    });
  };

  const onChange = e =>
    setQueryData({ ...queryData, [e.target.name]: e.target.value });

  let { queryTitle, queryCategory } = queryData;

  // Construct JSX with notebooks filtered
  let notebooksJSX = notebooks.map(notebook => {
    let match = false;
    let notebookFiltered = notebook;
    if (queryTitle.length > 0) {
      match = true;
      if (notebook.title.includes(queryTitle)) {
        notebookFiltered = notebook;
      } else {
        notebookFiltered = null;
        match = false;
      }
    }
    if (queryCategory.length > 0) {
      if (notebook.category.includes(queryCategory)) {
        notebookFiltered = notebook;
      } else if (!match) {
        notebookFiltered = null;
      }
    }

    if (notebookFiltered) {
      return (
        <NotebookItem
          key={notebook.id}
          notebook={notebookFiltered}
          onSelection={onSelectNotebookForShare}
        />
      );
    } else {
      return null;
    }
  });

  let shareFormJSX =
    queryData.selectedNotebookForShare !== null ? (
      <NotebookShareForm
        notebook={queryData.selectedNotebookForShare}
        onSave={onShareSaved}
      />
    ) : null;

  return loading ? (
    <Spinner />
  ) : (
    <section className='notebooks'>
      <h1>Notebooks</h1>
      <NotebookForm />
      <div className='notebooks__search-form'>
        <i className='fas fa-search'></i>
        Filter:
        <input
          type='text'
          name='queryTitle'
          onChange={e => onChange(e)}
          placeholder='Title'
        />
        <input
          type='text'
          name='queryCategory'
          onChange={e => onChange(e)}
          placeholder='Category'
        />
      </div>

      <div className='notebooks__list'>
        <div className='notebooks__list__labels'>
          <button className='notebooks__list__labels--title'>Title</button>
          <button className='notebooks__list__labels--category'>
            Category
          </button>
          <button className='notebooks__list__labels--created'>Created</button>
          <button className='notebooks__list__labels--updated'>
            Last modified
          </button>
          <button className='notebooks-list__labels--actions'>Actions</button>
        </div>
        {notebooksJSX}
      </div>

      <div className='share-nb'>{shareFormJSX}</div>
    </section>
  );
};

Notebooks.propTypes = {
  getNotebooks: PropTypes.func.isRequired,
  notebook: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  notebook: state.notebook
});

export default connect(mapStateToProps, { getNotebooks })(Notebooks);
