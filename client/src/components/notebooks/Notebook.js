import React, { useEffect } from "react";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { getNotebook } from "../../actions/notebook";
import { getNotes } from "../../actions/note";
import Spinner from "../layout/Spinner";
import NoteItem from "../notes/NoteItem";

const Notebook = ({
  getNotebook,
  getNotes,
  notebook: { notebook, loading },
  note: { notes, loading2 },
  match,
  auth
}) => {
  useEffect(() => {
    getNotebook(match.params.title);
  }, [getNotebook, match.params.title]);

  useEffect(() => {
    getNotes(match.params.title);
  }, [getNotes, match.params.title]);

  let notesJSX = notes.map(n => {
    return (
      <NoteItem
        key={n.id}
        notebookTitle={match.params.title}
        userId={auth.user.id}
        note={n}
      />
    );
  });

  return loading ? (
    <Spinner />
  ) : (
    <section className='notebook--single'>
      <div className='notebook--single__info'>
        <h1>Notebook {match.params.title}</h1>
      </div>
      <Link
        className='notebook--single__add'
        to={`/notebooks/${match.params.title}/notes/create`}
      >
        <i className='fas fa-plus'></i>
      </Link>
      <div className='notebook--single__list'>
        <div className='notebook--single__list__labels'>
          <button className='notebook--single__list__labels--nr'>
            Nr.crt.
          </button>
          <button className='notebook--single__list__labels--created'>
            Created
          </button>
          <button className='notebook--single__list__labels--updated'>
            Last modified
          </button>
          <button className='notebook--single-list__labels--actions'>
            Actions
          </button>
        </div>
        {notesJSX}
      </div>
    </section>
  );
};

Notebook.propTypes = {
  getNotebook: PropTypes.func.isRequired,
  getNotes: PropTypes.func.isRequired,
  notebook: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  notebook: state.notebook,
  note: state.note,
  auth: state.auth
});

export default connect(mapStateToProps, { getNotebook, getNotes })(Notebook);
