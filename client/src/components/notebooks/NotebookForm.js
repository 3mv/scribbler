import React, { useState } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { addNotebook } from "../../actions/notebook";

const NotebookForm = ({ addNotebook }) => {
  const [notebookData, setNotebookData] = useState({
    title: "",
    category: ""
  });

  const onChange = e =>
    setNotebookData({ ...notebookData, [e.target.name]: e.target.value });

  let { title, category } = notebookData;

  return (
    <div className='notebooks__form'>
      <form
        onSubmit={e => {
          e.preventDefault();
          addNotebook(notebookData);
          setNotebookData({
            title: "",
            category: ""
          });
        }}
      >
        <div className='notebooks__form__group'>
          <input
            type='text'
            name='title'
            id='title'
            value={title}
            onChange={e => onChange(e)}
            placeholder='Title'
            required
          />
        </div>
        <div className='notebooks__form__group'>
          <input
            type='text'
            name='category'
            id='category'
            value={category}
            onChange={e => onChange(e)}
            placeholder='Category'
            required
          />
        </div>
        <input
          type='submit'
          className='btn btn--primary'
          value='Add Notebook'
        />
      </form>
    </div>
  );
};

NotebookForm.propTypes = {
  addNotebook: PropTypes.func.isRequired
};

export default connect(null, { addNotebook })(NotebookForm);
