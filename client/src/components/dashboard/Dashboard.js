import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import Spinner from "../layout/Spinner";
import { updateUser } from "../../actions/auth";

class Dashboard extends Component {
  constructor(props) {
    super(props);

    this.state = {
      name: "",
      email: "",
      password: "",
      confirmPassword: "",
      readOnly: true,
      btnEditClassName: "",
      btnSubmitClassName: "hidden"
    };
  }

  onChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  onBtnEditHandler = () => {
    this.setState({
      readOnly: !this.state.readOnly,
      btnEditClassName:
        this.state.btnEditClassName === "hidden" ? "" : "hidden",
      btnSubmitClassName:
        this.state.btnSubmitClassName === "hidden" ? "" : "hidden"
    });
  };

  onFormSubmit = e => {
    e.preventDefault();

    this.setState({
      readOnly: !this.state.readOnly,
      btnEditClassName:
        this.state.btnEditClassName === "hidden" ? "" : "hidden",
      btnSubmitClassName:
        this.state.btnSubmitClassName === "hidden" ? "" : "hidden",
      password: "",
      confirmPassword: ""
    });

    // update user
    const userData = {
      name: this.state.name,
      email: this.state.email,
      password: this.state.password
    };
    this.props.updateUser(userData);
  };

  componentDidMount = () => {
    const { auth } = this.props;

    // TODO: this is not correct, change if possible
    if (!auth.loading) {
      this.setState({
        name: auth.user.name,
        email: auth.user.email,
        password: "",
        confirmPassword: ""
      });
    }
  };

  render() {
    return this.props.auth.loading ? (
      <Spinner />
    ) : (
      <section className='dashboard'>
        <h1>Dashboard</h1>
        <div className='dashboard__user-form'>
          <h2>Your credentials</h2>
          <form className='form' onSubmit={this.onFormSubmit}>
            <div className='form__group'>
              <input
                type='text'
                name='name'
                value={this.state.name}
                placeholder='Name'
                readOnly={this.state.readOnly}
                onChange={this.onChange}
                required
              />
            </div>
            <div className='form__group'>
              <input
                type='text'
                name='email'
                value={this.state.email}
                placeholder='Email'
                readOnly={this.state.readOnly}
                onChange={this.onChange}
                required
              />
            </div>
            <div className='form__group'>
              <input
                type='password'
                name='password'
                value={this.state.password}
                placeholder='Password'
                readOnly={this.state.readOnly}
                onChange={this.onChange}
                required
              />
            </div>
            <div className='form__group'>
              <input
                type='password'
                name='confirmPassword'
                value={this.state.confirmPassword}
                placeholder='Confirm Password'
                readOnly={this.state.readOnly}
                onChange={this.onChange}
                required
              />
            </div>
            <input
              type='submit'
              value='Save'
              className={"btn btn--primary " + this.state.btnSubmitClassName}
            />
          </form>
          <button
            className={"btn btn--light " + this.state.btnEditClassName}
            onClick={this.onBtnEditHandler}
          >
            Edit credentials
          </button>
        </div>
      </section>
    );
  }
}

Dashboard.propTypes = {
  auth: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth
});

export default connect(mapStateToProps, { updateUser })(Dashboard);
