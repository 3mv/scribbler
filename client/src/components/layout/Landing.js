import React from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import Spinner from "./Spinner";

const Landing = ({ auth: { isAuthenticated, loading } }) => {
  let buttonsJSX = null;

  if (!isAuthenticated) {
    buttonsJSX = (
      <div className='buttons'>
        <Link to='/register' className='btn btn--primary btn--big'>
          Sign Up
        </Link>
        <Link to='/login' className='btn btn--light btn--big'>
          Login
        </Link>
      </div>
    );
  }

  return loading ? (
    <Spinner />
  ) : (
    <section className='landing'>
      <div className='landing__inner'>
        <h1>Scribbler</h1>
        <p>
          Create notes, review them in time of need and share them with your
          friends
        </p>
        {buttonsJSX}
      </div>
    </section>
  );
};

Landing.propTypes = {
  auth: PropTypes.object
};

const mapStateToProps = state => ({
  auth: state.auth
});

export default connect(mapStateToProps, {})(Landing);
