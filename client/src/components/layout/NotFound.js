import React from "react";

const NotFound = () => {
  return (
    <section className='not-found'>
      <div>
        <i className='fas fa-exclamation-circle'></i>
        <strong>404</strong> Page Not Found
      </div>
      <div>
        <p>Are you serious?..</p>
      </div>
    </section>
  );
};

export default NotFound;
