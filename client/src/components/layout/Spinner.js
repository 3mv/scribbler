import React, { Fragment } from "react";
import spinner from "../../img/spinner.gif";

export default () => {
  return (
    <Fragment>
      <img
        src={spinner}
        style={{
          width: "100px",
          height: "auto",
          margin: "auto",
          display: "block"
        }}
        alt='Loading...'
      />
    </Fragment>
  );
};
