import React from "react";

const About = () => {
  return (
    <section className='about'>
      <h1>About</h1>
      <p>
        Final project for the web technologies lab at{" "}
        <a
          href='http://csie.ase.ro/home'
          rel='noopener noreferrer'
          target='_blank'
        >
          Faculty of Cybernetics Statistics and Economic Informatics
        </a>
      </p>

      <div className='about__team'>
        <h4>
          Team name: <span>3MV</span>
        </h4>
        <h4>Team composition:</h4>
        <ul>
          <li>
            <span>Vlad Puscaru</span> - Team Leader, Project Manager, Lead
            backend developer
          </li>
          <li>
            <span>Mihaela Popescu</span> - Lead frontend developer, Database
            architect
          </li>
          <li>
            <span>Madalina Popescu</span> - Backend assistant, Research Manager,
            Assistant Project Manager
          </li>
          <li>
            <span>Maria Cojocaru</span> - Frontend assistant, In charge of
            design mockups, Copyrighter
          </li>
        </ul>
      </div>

      <div className='about__project'>
        <h4>Project requirements</h4>
        <div className='about__project__info'>
          <h2>Web app for taking notes during courses/labs</h2>
          <h3>Goal</h3>
          <p>
            To develop a web app that allows students to take notes during
            courses/labs
          </p>
          <h3>Description</h3>
          <p>
            The application must allow students to organise their notes by
            classes they attend and individual study activities.
            <br /> The platform is based on a Single Page Application
            architecture. It will be accessible on a desktop, mobile or tablet
            browser (depending on user preferences). <br /> The note editor
            should be easy to use so that the student can take notes during the
            class. It will implement a markdown sistem to allow simple text
            formatting.
          </p>
          <h3>Minimal functionalities</h3>
          <ul>
            <li>
              As a student I want to be able to login with my institutional
              account (@stud.ase.ro) to manage my notes.
            </li>
            <li>
              As a student I want to be able to view, add, edit and delete notes
              so that I can better organize information
            </li>
            <li>
              As a student I want the option to add attachments (images,
              documents) to the notes I take so that I can have more details
              about the subject
            </li>
            <li>
              As a student I want to be able to organize notes based on classes,
              date, labels (tags) and keywords so that I can retrieve
              information easily
            </li>
            <li>
              As a student I want to be able to share notes with other
              colleagues
            </li>
            <li>
              As a student I want to be able to integrate content from other
              sources and take notes (like for example while I listen to a
              youtube video, i read a book on kindle, attending an online
              conference)
            </li>
            <li>
              As a student I want to be able to organize a study group. I can
              invite multiple colleagues that will share notes within the group
            </li>
          </ul>
          <h3>Examples</h3>
          <a
            href='https://www.studocu.com/'
            rel='noopener noreferrer'
            target='_blank'
          >
            Studocu
          </a>
          <a
            href='https://evernote.com/'
            rel='noopener noreferrer'
            target='_blank'
          >
            Evernote
          </a>
        </div>
      </div>
    </section>
  );
};

export default About;
