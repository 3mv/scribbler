import React, { Component } from "react";
import FroalaEditor from "react-froala-wysiwyg";
import { connect } from "react-redux";
import { getNote } from "../../actions/note";
import "froala-editor/js/froala_editor.pkgd.min.js";
import "froala-editor/css/froala_style.min.css";
import "froala-editor/css/froala_editor.pkgd.min.css";
import Spinner from "../layout/Spinner";

class NoteEditor extends Component {
  constructor(props) {
    super(props);

    this.state = {
      model: ""
    };
  }

  componentDidMount() {
    if (this.props.update) {
      this.props
        .getNote(this.props.match.params.title, this.props.match.params.id)
        .then(() => {
          this.setState({
            model: this.props.note.note.content
          });
        });
    }
  }

  handleModelChange = model => {
    this.setState({
      model: model
    });

    if (this.props.create) {
      this.props.create(this.state.model);
    }

    if (this.props.update) {
      this.props.update(this.state.model);
    }
  };

  render() {
    return this.props.create === null && this.props.note.loading ? (
      <Spinner />
    ) : (
      <FroalaEditor
        model={this.state.model}
        onModelChange={this.handleModelChange}
      />
    );
  }
}

const mapStateToProps = state => ({
  note: state.note
});

export default connect(mapStateToProps, { getNote })(NoteEditor);
