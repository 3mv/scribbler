import React, { Fragment, useState, useEffect } from "react";
import { Redirect } from "react-router-dom";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { getNotebooks } from "../../actions/notebook";
import { addNote } from "../../actions/note";
import { setAlert } from "../../actions/alert";
import NoteEditor from "./NoteEditor";
import Spinner from "../layout/Spinner";

const NoteCreate = ({
  getNotebooks,
  addNote,
  setAlert,
  notebook: { notebooks, loading },
  match
}) => {
  useEffect(() => {
    getNotebooks();
  }, [getNotebooks]);

  const [noteData, setNoteData] = useState({
    content: "",
    // selectedNotebook: "Select",
    created: false
  });

  // const onChange = e => {
  //   setNoteData({ ...noteData, selectedNotebook: e.target.value });
  // };

  const createNote = () => {
    addNote(noteData.content, match.params.title);
    setNoteData({ ...noteData, created: true });
  };

  const updateContent = content => {
    setNoteData({ ...noteData, content: content });
  };

  let selectNotebookData = notebooks.map(n => (
    <option key={n.id} value={n.title}>
      {n.title}
    </option>
  ));

  return noteData.created ? (
    <Redirect to={`/notebooks`} />
  ) : (
    <Fragment>
      <button
        onClick={() => createNote()}
        type='button'
        className='btn btn-primary'
      >
        Create Note
      </button>
      {/* <select onChange={e => onChange(e)}>
        <option value='Select'>Select</option>
        {selectNotebookData}
      </select> */}
      <NoteEditor content={noteData.content} create={updateContent} />
    </Fragment>
  );
};

NoteCreate.propTypes = {
  getNotebooks: PropTypes.func.isRequired,
  addNote: PropTypes.func.isRequired,
  setAlert: PropTypes.func.isRequired,
  notebook: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  notebook: state.notebook
});

export default connect(mapStateToProps, { getNotebooks, addNote, setAlert })(
  NoteCreate
);
