import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import { getNote, updateNote } from "../../actions/note";
import { connect } from "react-redux";
import NoteEditor from "./NoteEditor";
import Spinner from "../layout/Spinner";

const Note = ({ getNote, updateNote, note: { note, loading }, match }) => {
  const [noteData, setNoteData] = useState({ content: "" });

  useEffect(() => {
    getNote(match.params.title, match.params.id);
  }, [getNote, match.params.title, match.params.id]);

  const update = content => {
    setNoteData({ ...noteData, content: content });
  };

  const save = () => {
    updateNote(noteData.content, match.params.title, note.id);
  };

  return loading ? (
    <Spinner />
  ) : (
    <section className='note--single'>
      <button onClick={save} type='button' className='btn btn--primary'>
        Save
      </button>
      <NoteEditor match={match} update={update} />
    </section>
  );
};

Note.propTypes = {
  getNote: PropTypes.func.isRequired,
  updateNote: PropTypes.func.isRequired,
  note: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  note: state.note
});

export default connect(mapStateToProps, { getNote, updateNote })(Note);
