import React, { Fragment } from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import Moment from "react-moment";
import { connect } from "react-redux";
import { deleteNote } from "../../actions/note";

const NoteItem = ({
  notebookTitle,
  userId,
  auth,
  deleteNote,
  note: { id, content, createdAt, updatedAt }
}) => {
  return (
    <div className='noteItem'>
      <span className='noteItem__data noteItem__data--nr'>
        <i className='far fa-file'></i> {id}
      </span>
      <span className='noteItem__data noteItem__data--created'>
        <Moment format='DD/MM/YYYY'>{createdAt}</Moment>
      </span>
      <span className='noteItem__data noteItem__data--updated'>
        <Moment format='DD/MM/YYYY'>{updatedAt}</Moment>
      </span>

      <span className='noteItem__data noteItem__data--buttons'>
        {!auth.loading && userId === auth.user.id && (
          <Fragment>
            <button onClick={e => deleteNote(notebookTitle, id)} type='button'>
              <i className='fas fa-trash-alt'></i>
            </button>
          </Fragment>
        )}
        <Link to={`/notebooks/${notebookTitle}/${id}`}>
          <i className='fas fa-book-open'></i>
        </Link>
      </span>
    </div>
  );
};

NoteItem.propTypes = {
  notebookTitle: PropTypes.string.isRequired,
  userId: PropTypes.number.isRequired,
  note: PropTypes.object.isRequired,
  auth: PropTypes.object.isRequired,
  deleteNote: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth
});

export default connect(mapStateToProps, { deleteNote })(NoteItem);
