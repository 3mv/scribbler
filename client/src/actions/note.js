import axios from "axios";
import { setAlert } from "./alert";
import {
  GET_NOTES,
  GET_NOTE,
  NOTE_ERROR,
  DELETE_NOTE,
  ADD_NOTE,
  UPDATE_NOTE
} from "./types";

// Get notes
export const getNotes = notebookTitle => async dispatch => {
  try {
    const res = await axios.get(`/api/notebooks/${notebookTitle}/notes`);

    dispatch({
      type: GET_NOTES,
      payload: res.data
    });
  } catch (err) {
    console.log(err.response);
    dispatch({
      type: NOTE_ERROR,
      payload: { msg: err.response.statusText, status: err.response.status }
    });
  }
};

// Get note
export const getNote = (notebookTitle, noteId) => async dispatch => {
  try {
    const res = await axios.get(
      `/api/notebooks/${notebookTitle}/notes/${noteId}`
    );

    dispatch({
      type: GET_NOTE,
      payload: res.data
    });
  } catch (err) {
    console.log(err.response);
    dispatch({
      type: NOTE_ERROR,
      payload: { msg: err.response.statusText, status: err.response.status }
    });
  }
};

// Delete note
export const deleteNote = (notebookTitle, noteId) => async dispatch => {
  try {
    await axios.delete(`/api/notebooks/${notebookTitle}/notes/${noteId}`);

    dispatch({
      type: DELETE_NOTE,
      payload: noteId
    });

    dispatch(setAlert("Note Removed", "success"));
  } catch (err) {
    dispatch({
      type: NOTE_ERROR,
      payload: { msg: err.response.statusText, status: err.response.status }
    });
  }
};

// Create and add note to notebook
export const addNote = (content, notebookTitle) => async dispatch => {
  try {
    const config = {
      headers: {
        "Content-Type": "application/json"
      }
    };

    const res = await axios.post(
      `/api/notebooks/${notebookTitle}/notes`,
      { content },
      config
    );

    dispatch({
      type: ADD_NOTE,
      payload: res.data
    });

    dispatch(setAlert("Note created", "succes"));
  } catch (err) {
    console.log(err.response);
    dispatch({
      type: NOTE_ERROR,
      payload: { msg: err.response.statusText, status: err.response.status }
    });
  }
};

// Update note
export const updateNote = (
  content,
  notebookTitle,
  noteId
) => async dispatch => {
  try {
    const config = {
      headers: {
        "Content-Type": "application/json"
      }
    };

    const res = await axios.put(
      `/api/notebooks/${notebookTitle}/notes/${noteId}`,
      { content },
      config
    );

    dispatch({
      type: UPDATE_NOTE,
      payload: res.data
    });

    dispatch(setAlert("Note updated", "succes"));
  } catch (err) {
    console.log(err.response);
    dispatch({
      type: NOTE_ERROR,
      payload: { msg: err.response.statusText, status: err.response.status }
    });
  }
};
