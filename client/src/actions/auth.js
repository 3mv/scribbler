import axios from "axios";
import { setAlert } from "./alert";
import setAuthToken from "../util/setAuthToken";
import {
  REGISTER_SUCCES,
  REGISTER_FAIL,
  USER_LOADED,
  AUTH_ERROR,
  LOGIN_SUCCES,
  LOGIN_FAIL,
  LOGOUT,
  USER_UPDATE_SUCCES,
  USER_UPDATE_FAIL,
  GET_USERS_SUCCES,
  GET_USERS_ERROR
} from "./types";

// Load User
export const loadUser = () => async dispatch => {
  // Check to see if a token is in global storage
  // and put it in a global header
  if (localStorage.token) {
    setAuthToken(localStorage.token);
  }

  try {
    const res = await axios.get("/api/users/auth");
    dispatch({
      type: USER_LOADED,
      payload: res.data
    });
  } catch (err) {
    dispatch({
      type: AUTH_ERROR
    });
  }
};

// Register User
export const register = ({ name, email, password }) => async dispatch => {
  const config = {
    headers: {
      "Content-Type": "application/json"
    }
  };

  const body = JSON.stringify({ name, email, password });

  try {
    const res = await axios.post("/api/users", body, config);

    dispatch({
      type: REGISTER_SUCCES,
      payload: res.data
    });

    dispatch(loadUser());
  } catch (err) {
    const errors = err.response.data.errors;

    if (errors) {
      errors.forEach(error => {
        dispatch(setAlert(error.msg, "danger"));
      });
    }

    dispatch({
      type: REGISTER_FAIL
    });
  }
};

// Login User
export const login = (email, password) => async dispatch => {
  const config = {
    headers: {
      "Content-Type": "application/json"
    }
  };

  const body = JSON.stringify({ email, password });

  try {
    const res = await axios.post("/api/users/auth", body, config);

    dispatch({
      type: LOGIN_SUCCES,
      payload: res.data
    });

    dispatch(loadUser());
  } catch (err) {
    const errors = err.response.data.errors;

    if (errors) {
      errors.forEach(error => {
        dispatch(setAlert(error.msg, "danger"));
      });
    }

    dispatch({
      type: LOGIN_FAIL
    });
  }
};

// Logout
export const logout = () => dispatch => {
  dispatch({ type: LOGOUT });
};

// Update user
export const updateUser = userData => async dispatch => {
  const config = {
    headers: {
      "Content-Type": "application/json"
    }
  };

  const body = userData;

  try {
    const res = await axios.put("/api/users/auth", body, config);

    dispatch({
      type: USER_UPDATE_SUCCES,
      payload: res.data
    });

    dispatch(setAlert("User succesfully updated", "succes"));
  } catch (err) {
    const errors = err.response.data.errors;

    if (errors) {
      errors.forEach(error => {
        dispatch(setAlert(error.msg, "danger"));
      });
    }

    dispatch({
      type: USER_UPDATE_FAIL
    });
  }
};

// Get All Users
export const getUsers = () => async dispatch => {
  try {
    const res = await axios.get("/api/users");
    dispatch({
      type: GET_USERS_SUCCES,
      payload: res.data
    });
  } catch (err) {
    dispatch({
      type: GET_USERS_ERROR
    });
  }
};
