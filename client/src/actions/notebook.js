import axios from "axios";
import { setAlert } from "./alert";
import {
  GET_NOTEBOOKS,
  NOTEBOOK_ERROR,
  DELETE_NOTEBOOK,
  ADD_NOTEBOOK,
  GET_NOTEBOOK,
  UPDATE_NOTEBOOK_SUCCES,
  UPDATE_NOTEBOOK_FAIL,
  UPDATE_NOTEBOOK_SHARE_SUCCES
} from "./types";

// Get Notebooks
export const getNotebooks = () => async dispatch => {
  try {
    const res = await axios.get("/api/notebooks");

    dispatch({
      type: GET_NOTEBOOKS,
      payload: res.data
    });
  } catch (err) {
    dispatch({
      type: NOTEBOOK_ERROR,
      payload: { msg: err.response.statusText, status: err.response.status }
    });
  }
};

// Delete Notebook
export const deleteNotebook = title => async dispatch => {
  try {
    await axios.delete(`/api/notebooks/${title}`);

    dispatch({
      type: DELETE_NOTEBOOK,
      payload: title
    });

    dispatch(setAlert("Notebook Removed", "success"));
  } catch (err) {
    const errors = err.response.data.errors;

    if (errors) {
      errors.forEach(error => {
        dispatch(setAlert(error.msg, "danger"));
      });
    }

    dispatch({
      type: NOTEBOOK_ERROR,
      payload: errors
    });
  }
};

// ADD Notebook
export const addNotebook = formData => async dispatch => {
  const config = {
    headers: {
      "Content-Type": "application/json"
    }
  };

  try {
    const res = await axios.post("/api/notebooks", formData, config);

    dispatch({
      type: ADD_NOTEBOOK,
      payload: res.data
    });

    dispatch(setAlert("Notebook Created", "success"));
  } catch (err) {
    const errors = err.response.data.errors;

    if (errors) {
      errors.forEach(error => {
        dispatch(setAlert(error.msg, "danger"));
      });
    }

    dispatch({
      type: NOTEBOOK_ERROR,
      payload: errors
    });
  }
};

// Get Notebooks
export const getNotebook = title => async dispatch => {
  try {
    const res = await axios.get(`/api/notebooks/${title}`);

    dispatch({
      type: GET_NOTEBOOK,
      payload: res.data
    });
  } catch (err) {
    const errors = err.response.data.errors;

    if (errors) {
      errors.forEach(error => {
        dispatch(setAlert(error.msg, "danger"));
      });
    }

    dispatch({
      type: NOTEBOOK_ERROR,
      payload: errors
    });
  }
};

// UPDATE Notebook
export const updateNotebook = formData => async dispatch => {
  const config = {
    headers: {
      "Content-Type": "application/json"
    }
  };

  try {
    const res = await axios.put(
      `/api/notebooks/${formData.id}`,
      formData,
      config
    );

    dispatch({
      type: UPDATE_NOTEBOOK_SUCCES,
      payload: res.data
    });

    dispatch(setAlert("Notebook Updated", "success"));
  } catch (err) {
    const errors = err.response.data.errors;

    if (errors) {
      errors.forEach(error => {
        dispatch(setAlert(error.msg, "danger"));
      });
    }

    dispatch({
      type: UPDATE_NOTEBOOK_FAIL
    });
  }
};

// UPDATE Notebook Shares
export const updateNotebookShare = formData => async dispatch => {
  const config = {
    headers: {
      "Content-Type": "application/json"
    }
  };

  console.log(formData);

  try {
    const res = await axios.put(
      `/api/notebooks/share/${formData.title}`,
      formData.users,
      config
    );

    dispatch({
      type: UPDATE_NOTEBOOK_SHARE_SUCCES,
      payload: res.data
    });

    dispatch(setAlert("Notebook Shared Preferences Updated", "success"));
  } catch (err) {
    const errors = err.response.data.errors;

    if (errors) {
      errors.forEach(error => {
        dispatch(setAlert(error.msg, "danger"));
      });
    }

    dispatch({
      type: UPDATE_NOTEBOOK_FAIL
    });
  }
};
