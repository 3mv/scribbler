import React, { Fragment, useEffect } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Breadcrumbs from "react-router-dynamic-breadcrumbs";

import { loadUser } from "./actions/auth";
import setAuthToken from "./util/setAuthToken";
import PrivateRoute from "./components/routing/PrivateRoute";
import Navbar from "./components/layout/Navbar";
import Landing from "./components/layout/Landing";
import Register from "./components/auth/Register";
import Login from "./components/auth/Login";
import Alert from "./components/layout/Alert";
import Dashboard from "./components/dashboard/Dashboard";
import Notebooks from "./components/notebooks/Notebooks";
import Notebook from "./components/notebooks/Notebook";
import Note from "./components/notes/Note";
import NoteCreate from "./components/notes/NoteCreate";
import About from "./components/layout/About";
import NotFound from "./components/layout/NotFound";

// Redux
import { Provider } from "react-redux";
import store from "./store";

// Style
// import "./App.css";
import "./sass/App.sass";

// Check to see if a token is in global storage
// and put it in a global header
if (localStorage.token) {
  setAuthToken(localStorage.token);
}

// Set up routes for breadcrumbs
const routes = {
  "/": "Home",
  "/register": "Register",
  "/login": "Login",
  "/about": "About",
  "/dashboard": "Dashboard",
  "/notebooks": "Notebooks",
  "/notebooks/:title": "Notebook :title",
  "/notebooks/:title/:id": "Note :id",
  "/notebooks/:title/notes": "",
  "/notebooks/:title/notes/create": "Create note"
};

const App = () => {
  // only runs once
  // similar to componentDidMount()
  useEffect(() => {
    store.dispatch(loadUser());
  }, []);

  return (
    <Provider store={store}>
      <Router>
        <Navbar />
        <Fragment>
          <Breadcrumbs
            WrapperComponent={props => (
              <ol className='breadcrumb'>{props.children}</ol>
            )}
            ActiveLinkComponent={props => (
              <li className='active'>{props.children}</li>
            )}
            LinkComponent={props => <li>{props.children}</li>}
            mappedRoutes={routes}
            routeMatcherRegex='([\w-]+)'
          />
          <div className='container'>
            <Alert />
            <Switch>
              <Route exact path='/' component={Landing} />
              <Route exact path='/about' component={About} />
              <Route exact path='/register' component={Register} />
              <Route exact path='/login' component={Login} />
              <PrivateRoute exact path='/dashboard' component={Dashboard} />
              <PrivateRoute exact path='/notebooks' component={Notebooks} />
              <PrivateRoute
                exact
                path='/notebooks/:title'
                component={Notebook}
              />
              <PrivateRoute
                exact
                path='/notebooks/:title/:id'
                component={Note}
              />
              <PrivateRoute
                exact
                path='/notebooks/:title/notes/create'
                component={NoteCreate}
              />
              <Route component={NotFound} />
            </Switch>
          </div>
        </Fragment>
      </Router>
    </Provider>
  );
};

export default App;
