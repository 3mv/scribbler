'use strict'

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('Note', {
    content: {
      type: DataTypes.STRING,
      default: 'New Note'
    },
    notebookId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'Notebooks',
        key: 'id'
      }
    }
  });
}