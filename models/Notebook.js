"use strict";

module.exports = function(sequelize, DataTypes) {
  return sequelize.define("Notebook", {
    title: {
      type: DataTypes.STRING,
      alowNull: false,
      unique: true
    },
    category: {
      type: DataTypes.STRING,
      allowNull: false
    },
    userId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: "Users",
        key: "id"
      }
    },
    shared: {
      type: DataTypes.STRING
    }
  });
};
