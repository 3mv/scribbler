const express = require("express");
const router = express.Router();
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const auth = require("../../middleware/auth");
const config = require("config");
const { check, validationResult } = require("express-validator");
const Sequelize = require("sequelize");
const Op = Sequelize.Op;

const User = require("../../config/database")["User"];

router.use(require("../../middleware/error"));

/**
 * @route   GET api/users/auth
 * @desc    Get authenticated user
 * @acces   Private
 */
router.get("/auth", auth, async (req, res, next) => {
  try {
    const user = await User.findByPk(req.user.id, {
      attributes: {
        exclude: ["password"]
      }
    });
    res.status(200).json(user);
  } catch (err) {
    next(err);
  }
});

/**
 * @route   POST api/users/auth
 * @desc    Authenticate user & get token (Login)
 * @acces   Public
 */
router.post(
  "/auth",
  [
    check("email", "Please include a valid email").isEmail(),
    check("password", "Password is required").exists()
  ],
  async (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json(errors);
    }

    const { email, password } = req.body;

    try {
      // Se if the user exists
      let user = await User.findOne({
        where: {
          email: email
        }
      });

      if (!user) {
        return res
          .status(400)
          .json({ errors: [{ msg: "Invalid credentials" }] });
      }

      // Check if the passwords match
      const match = await bcrypt.compare(password, user.password);

      if (!match) {
        return res
          .status(400)
          .json({ errors: [{ msg: "Invalid credentials" }] });
      }

      // Return JSON Web Token
      const payload = {
        user: {
          id: user.id
        }
      };

      jwt.sign(
        payload,
        config.get("jwtSecret"),
        {
          expiresIn: 360000
        },
        (err, token) => {
          if (err) {
            throw err;
          }
          res.json({ token });
        }
      );
    } catch (err) {
      next(err);
    }
  }
);

/**
 * @route   POST api/users
 * @desc    Register user
 * @acces   Public
 */
router.post(
  "/",
  [
    check("name", "Name is required")
      .not()
      .isEmpty(),
    check("email", "Please include a valid email address").isEmail(),
    check(
      "password",
      "Please enter a password with 6 or more characters"
    ).isLength({ min: 6 })
  ],
  async (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json(errors);
    }

    const { name, email, password } = req.body;

    try {
      // Check if the users already exists
      let user = await User.findOne({
        where: {
          email: email
        }
      });
      if (user) {
        return res
          .status(401)
          .json({ errors: [{ msg: "User already exists" }] });
      }

      // Encrypt the password
      const salt = await bcrypt.genSalt(10);
      const encryptedPassword = await bcrypt.hash(password, salt);

      // Save user to database
      user = {
        name: name,
        email: email,
        password: encryptedPassword
      };
      await User.create(user);

      const id = await User.findOne({
        where: {
          email: user.email
        }
      }).get("id");

      // Return JSON Web Token
      const payload = {
        user: {
          id: id
        }
      };
      jwt.sign(
        payload,
        config.get("jwtSecret"),
        {
          expiresIn: 360000
        },
        (err, token) => {
          if (err) {
            throw err;
          }
          res.json({ token });
        }
      );
    } catch (err) {
      next(err);
    }
  }
);

/**
 * @route   PUT api/users/auth
 * @desc    Update authenticated user
 * @acces   Private
 */
router.put(
  "/auth",
  [
    auth,
    [
      check("name", "Please include a name").exists(),
      check("email", "Please include a valid email").isEmail(),
      check("password", "Password is required").exists()
    ]
  ],
  async (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json(errors);
    }

    const { name, email, password } = req.body;

    try {
      // Take authenticated user
      const user = await User.findByPk(req.user.id);

      // Check if the passwords match
      const match = await bcrypt.compare(password, user.password);

      if (!match) {
        return res
          .status(400)
          .json({ errors: [{ msg: "Invalid credentials" }] });
      }

      // Update the user
      await User.update(
        {
          name: name,
          email: email
        },
        {
          where: {
            id: user.id
          }
        }
      );

      const updatedUser = await User.findByPk(user.id, {
        attributes: {
          exlude: ["password"]
        }
      });

      res.status(200).json(updatedUser);
    } catch (err) {
      next(err);
    }
  }
);

/**
 * @route   GET api/users
 * @desc    Get all users besides authenticated one
 * @acces   Private
 */
router.get("/", auth, async (req, res, next) => {
  try {
    const users = await User.findAll({
      where: {
        id: {
          [Op.ne]: req.user.id
        }
      },
      attributes: {
        exclude: ["password", "email", "createdAt", "updatedAt"]
      }
    });
    res.status(200).json(users);
  } catch (err) {
    next(err);
  }
});

module.exports = router;
