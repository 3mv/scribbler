const express = require("express");
const router = express.Router();
const auth = require("../../middleware/auth");
const { check, validationResult } = require("express-validator");

const Notebook = require("../../config/database")["Notebook"];
const Note = require("../../config/database")["Note"];
const User = require("../../config/database")["User"];

/**
 * @router    Get notes for current notebook
 * @access    Private
 */
router.use("/:title/notes", [
  auth,
  async (req, res, next) => {
    try {
      let notebook = await Notebook.findOne({
        where: {
          userId: req.user.id,
          title: req.params.title
        }
      });

      if (!notebook) {
        return res.status(404).json({
          errors: [{ msg: `Notebook ${req.params.title} not found` }]
        });
      } else {
        req.notebook = notebook;
        next();
      }
    } catch (err) {
      next(err);
    }
  },
  require("./notes")
]);

/**
 * @route       GET api/notebooks
 * @desc        Get current user's notebooks (Also shared ones by others)
 * @queryParams "category"
 * @access      Private
 */
router.get("/", auth, async (req, res, next) => {
  try {
    let notebooks = [];
    if (req.query.category) {
      notebooks = await Notebook.findAll({
        where: {
          category: req.query.category,
          userId: req.user.id
        }
      });
    } else {
      notebooks = await Notebook.findAll({
        where: {
          userId: req.user.id
        }
      });

      // get shared ones
      await Notebook.findAll().map(n => {
        if (n.shared !== null) {
          let sharedIds = n.shared.split(",");
          sharedIds.forEach(s => {
            if (parseInt(s) === req.user.id) {
              notebooks.push(n);
            }
          });
        }
      });
    }

    console.log(notebooks);

    res.status(200).json(notebooks);
  } catch (err) {
    next(err);
  }
});

/**
 * @route   POST api/notebooks
 * @desc    Create a notebook for the current user
 * @access  Private
 */
router.post(
  "/",
  [
    auth,
    [
      check("title", "Title is required")
        .not()
        .isEmpty(),
      check("category", "Category is required")
        .not()
        .isEmpty()
    ]
  ],
  async (req, res, next) => {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(400).json(errors);
      }

      const { title, category } = req.body;
      // check title unique
      let notebook = await Notebook.findOne({
        where: {
          title: title
        }
      });

      if (notebook) {
        return res
          .status(400)
          .json({ errors: [{ msg: `Notebook ${title} already exists` }] });
      }

      // create notebook
      notebook = {
        title: title,
        category: category,
        userId: req.user.id
      };
      notebook = await Notebook.create(notebook);

      res.status(201).json(notebook);
    } catch (err) {
      next(err);
    }
  }
);

/**
 * @route   GET api/notebooks/:title
 * @desc    Get notebook by title (for authenticated user)
 * @acces   Private
 */
router.get("/:title", auth, async (req, res, next) => {
  try {
    let notebook = await Notebook.findOne({
      where: {
        userId: req.user.id,
        title: req.params.title
      }
    });

    if (!notebook) {
      return res
        .status(404)
        .json({ errors: [{ msg: `Notebook ${req.params.title} not found` }] });
    }

    res.status(202).json(notebook);
  } catch (err) {
    next(err);
  }
});

/**
 * @route   DELETE api/notebooks/:title
 * @desc    Delete notebook by title (for authenticated user)
 * @acces   Private
 */
router.delete("/:title", auth, async (req, res, next) => {
  try {
    let notebook = await Notebook.findOne({
      where: {
        title: req.params.title
      }
    });

    if (!notebook) {
      return res
        .status(404)
        .json({ errors: [{ msg: `Notebook ${req.params.title} not found` }] });
    }

    await notebook.destroy();

    res.status(203).json({ msg: `Notebook ${req.params.title} deleted` });
  } catch (err) {
    next(err);
  }
});

/**
 * @route   PUT api/notebooks/:id
 * @desc    Update notebook by id (for authenticated user)
 * @acces   Private
 */
router.put("/:id", auth, async (req, res, next) => {
  try {
    let notebook = await Notebook.findByPk(req.params.id);

    if (!notebook) {
      return res
        .status(404)
        .json({ errors: [{ msg: `Notebook ${req.params.title} not found` }] });
    }

    if (Object.keys(req.body).length > 0) {
      await notebook.update(req.body);
    }

    res.status(203).json(notebook);
  } catch (err) {
    next(err);
  }
});

/**
 * @route   PUT api/notebooks/share/:title
 * @desc    Update notebook shareable preferences (for authenticated user)
 * @acces   Private
 */
router.put("/share/:title", auth, async (req, res, next) => {
  try {
    let notebook = await Notebook.findOne({
      where: {
        title: req.params.title
      }
    });

    if (!notebook) {
      return res
        .status(404)
        .json({ errors: [{ msg: `Notebook ${req.params.title} not found` }] });
    }

    // construct shareable string
    let shareString = "";
    let wentIn = [];
    req.body.forEach((u, index) => {
      console.log("ASTA - " + typeof u);
      // see if it is already shared
      let isShared = false;
      if (notebook.shared !== null) {
        notebook.shared.split(",").map(s => {
          if (s === u) {
            isShared = true;
          }
        });
      }
      if (wentIn.length > 0) {
        wentIn.map(w => {
          if (w === u) {
            isShared = true;
          }
        });
      }
      if (!isShared) {
        shareString += u;
        wentIn.push(u);
        if (index !== req.body.length - 1) {
          shareString += ",";
        }
      }
    });

    if (notebook.shared !== null) {
      await notebook.update({
        shared: notebook.shared + "," + shareString
      });
    } else {
      await notebook.update({
        shared: shareString
      });
    }

    res.status(203).json(notebook);
  } catch (err) {
    next(err);
  }
});

module.exports = router;
