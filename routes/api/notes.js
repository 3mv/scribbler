"use strict";
const express = require("express");
const router = express.Router();
const { check, validationResult } = require("express-validator");

const Note = require("../../config/database")["Note"];

/**
 * @route       GET api/notebooks/:title/notes
 * @desc        Get notebook's ':title' notes
 * @access      Private
 */
router.get("/", async (req, res, next) => {
  try {
    let notes = await Note.findAll({
      where: {
        notebookId: req.notebook.id
      }
    });

    res.status(200).json(notes);
  } catch (err) {
    next(err);
  }
});

/**
 * @route       POST api/notebooks/:title/notes
 * @desc        Create a new note for notebook ':title'
 * @access      Private
 */
router.post(
  "/",
  [
    check("content", "Content is required")
      .not()
      .isEmpty()
  ],
  async (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json(errors);
    }

    try {
      const note = await Note.create({
        content: req.body.content,
        notebookId: req.notebook.id
      });

      res.status(200).json(note);
    } catch (err) {
      next(err);
    }
  }
);

/**
 * @route       GET api/notebooks/:title/notes/:id
 * @desc        Get note with id ':id' from notebook ':title'
 * @access      Private
 */
router.get("/:id", async (req, res, next) => {
  try {
    let note = await Note.findOne({
      where: {
        notebookId: req.notebook.id,
        id: req.params.id
      }
    });

    if (!note) {
      return res
        .status(404)
        .json({ errors: [{ msg: `Note ${req.params.id} not found` }] });
    }

    res.status(201).json(note);
  } catch (err) {
    next(err);
  }
});

/**
 * @route       PUT api/notebooks/:title/notes/:id
 * @desc        Update note with id ':id' from notebook ':title'
 * @access      Private
 */
router.put("/:id", async (req, res, next) => {
  try {
    let note = await Note.findOne({
      where: {
        notebookId: req.notebook.id,
        id: req.params.id
      }
    });

    if (!note) {
      return res
        .status(404)
        .json({ errors: [{ msg: `Note ${req.params.id} not found` }] });
    }

    await note.update(req.body);

    res.status(201).json({ msg: "Note updated" });
  } catch (err) {
    next(err);
  }
});

/**
 * @route       DELETE api/notebooks/:title/notes/:id
 * @desc        Delete note with id ':id' from notebook ':title'
 * @access      Private
 */
router.delete("/:id", async (req, res, next) => {
  try {
    let note = await Note.findOne({
      where: {
        notebookId: req.notebook.id,
        id: req.params.id
      }
    });

    if (!note) {
      return res
        .status(404)
        .json({ errors: [{ msg: `Note ${req.params.id} not found` }] });
    }

    await note.destroy();

    res.status(201).json({ msg: "Note deleted" });
  } catch (err) {
    next(err);
  }
});

module.exports = router;
